library ieee;
use ieee.std_logic_1164.all;

library vunit_lib;
context vunit_lib.vunit_context;

entity blinky_tb is
  generic (runner_cfg : string);
end blinky_tb;

architecture behave of blinky_tb is
  signal clk, nrst : std_logic := '0';
  signal led_o : std_logic_vector(7 downto 0);
begin

  clk <= not clk after 5 ns;
  nrst <= '0', '1' after 30 ns;

  main : process
  begin
    test_runner_setup(runner, runner_cfg);
    report "Hello World";
    wait until led_o = x"0f";
    test_runner_cleanup(runner);
  end process; -- main

  dut : entity work.blinky
  port map (
    clk => clk,
    nrst => nrst,
    led => led_o
  );

end behave;
