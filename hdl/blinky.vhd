library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity blinky is
  Port (
    clk : in std_logic;
    nrst : in std_logic;
    led : out std_logic_vector(7 downto 0)
  );
end blinky;

architecture behave of blinky is
  signal cnt : unsigned(7 downto 0);
begin

  led <= std_logic_vector(cnt);

  process(clk)
  begin
    if nrst = '0' then
      cnt <= (others => '0');
    elsif rising_edge(clk) then
      if cnt < 16 then
        cnt <= cnt + 1;
      end if;
    end if;

  end process;

end behave;
