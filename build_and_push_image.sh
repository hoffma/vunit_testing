#!/bin/bash

docker login registry.gitlab.com
docker build -t registry.gitlab.com/hoffma/vunit_testing .
docker push registry.gitlab.com/hoffma/vunit_testing
